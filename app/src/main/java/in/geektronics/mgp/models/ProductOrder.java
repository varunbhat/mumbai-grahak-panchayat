package in.geektronics.mgp.models;


import android.text.format.Time;

import com.orm.SugarRecord;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ABHIJEET on 08-01-2015.
 */
public class ProductOrder extends SugarRecord<ProductOrder> {
    User user;
    Product product;
    int quantity;
    Date time;

    public ProductOrder(){}

    public ProductOrder(User user, Product product, int quantity){
        this.user = user;
        this.product = product;
        this.quantity = quantity;
        time = new Date();
        time.setTime(System.currentTimeMillis());
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Product getProduct() {
        return product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
