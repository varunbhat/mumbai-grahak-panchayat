package in.geektronics.mgp.models;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ABHIJEET on 22-01-2015.
 */
public class User extends SugarRecord <User> {
    private static final String JSON_KEY_MEMBER_ID = "uid";
    private static final String JSON_KEY_MEMBER_NAME = "name";
    private static final String JSON_KEY_MEMBER_GENDER = "gender";
    private static final String JSON_KEY_MEMBER_YOB = "yob";
    private static final String JSON_KEY_MEMBER_CARE_OF = "co";
    private static final String JSON_KEY_MEMBER_ADDRESS = "address";
    private static final String JSON_KEY_MEMBER_LOCATION = "location";
    private static final String JSON_KEY_MEMBER_MOBILE = "mobile";

    String uid;
    String name;
    String gender;
    String yob;
    String co;
    String address;
    String location;
    String mobile;
    String dob;

    Boolean isUserVerified;


    @Ignore
    private String image;

    @Ignore
    private boolean selected;

    public void setDob(String dob){
        this.dob = dob;
    }

    public  String getDob(){
        return this.dob;
    }

    public User() {
    }

    public User(String name,String number){
        this.name = name;
        this.mobile = number;
        this.selected = false;
        this.isUserVerified = false;
    }

    public User(String name, String gender, String yob, String location, String mobile) {
        this.name = name;
        this.gender = gender;
        this.yob = yob;
        this.location = location;
        this.mobile = mobile;
    }

    // For returning JSON object
    public static User fromJson(JSONObject jsonObject) {
        User u = new User();
        try {
            // Deserialize json into object fields
            u.setUid(jsonObject.getString(JSON_KEY_MEMBER_ID));
            u.setName(jsonObject.getString(JSON_KEY_MEMBER_NAME));
            u.setGender(jsonObject.getString(JSON_KEY_MEMBER_GENDER));
            u.setCo(jsonObject.getString(JSON_KEY_MEMBER_CARE_OF));
            u.setAddress(jsonObject.getString(JSON_KEY_MEMBER_ADDRESS));
            u.setYob(jsonObject.getString(JSON_KEY_MEMBER_YOB));
            u.setLocation(jsonObject.getString(JSON_KEY_MEMBER_LOCATION));
            u.setMobile(jsonObject.getString(JSON_KEY_MEMBER_MOBILE));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        // Return new object
        return u;
    }

    public static ArrayList<User> fromJson(JSONArray jsonArray) {
        ArrayList<User> users = new ArrayList<User>(jsonArray.length());
        // Process each result in json array, decode and convert to groups
        // object
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject userJson = null;
            try {
                userJson = jsonArray.getJSONObject(i);
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }

            User user = User.fromJson(userJson);
            if (user != null) {
                users.add(user);
            }
        }
        return users;
    }


    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getYob() {
        return yob;
    }

    public void setYob(String yob) {
        this.yob = yob;
    }

    public String getCo() {
        return co;
    }

    public void setCo(String co) {
        this.co = co;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Boolean getIsUserVerified() {
        return isUserVerified;
    }

    public void setIsUserVerified(Boolean isUserVerified) {
        this.isUserVerified = isUserVerified;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", gender='" + gender + '\'' +
                ", location='" + location + '\'' +
                ", address='" + address + '\'' +
                ", yob='" + address + '\'' +
                ", mobile='" + mobile + '\'' +
                '}';
    }
}
