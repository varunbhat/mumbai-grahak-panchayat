package in.geektronics.mgp.models;

import android.util.Log;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

/**
 * Created by ABHIJEET on 07-01-2015.
 */
public class Product extends SugarRecord<Product> {
    ProductCategory category;
    private String englishName;
    private String marathiName;
    private String nextMonth;
    private String quantity;
    private String price;

    @Ignore
    Boolean addProductCategoryFlag = false;

    @Ignore
    Boolean addProductFlag = true;

    @Ignore
    int productCount;

    public Product(){}

    public int getProductCount(){
        return productCount;
    }

    public void setProductCount(int productCount) {
        this.productCount = productCount;
    }


    public Product(String category, String englishName, String marathiName, String nextMonth, String quantity, String price){
        List<ProductCategory> lProductCatagory = ProductCategory.find(ProductCategory.class, "group_name=?", category);
        if(lProductCatagory.size() == 0 ) {
            this.category = new ProductCategory(category, "");
            this.addProductCategoryFlag = true;
        }
        else
            this.category = lProductCatagory.get(0);

        this.englishName = englishName;
        this.marathiName = marathiName;
        this.nextMonth = nextMonth;
        this.quantity = quantity;
        this.price = price;
        this.productCount = 0;

        List <Product> lResult = Select.from(Product.class).where(Condition.prop("english_name").eq(englishName),
                Condition.prop("marathi_name").eq(marathiName),
                Condition.prop("next_month").eq(nextMonth),
                Condition.prop("quantity").eq(quantity),
                Condition.prop("price").eq(price)).list();

        if(lResult.size() != 0){
            addProductFlag = false;
        }
        Log.d("Debugging", "Category added to :" + this.category.getProductCategory());
    }

    public Product(String[] attributes){
        List<ProductCategory> lProductCatagory = ProductCategory.find(ProductCategory.class, "product_category=?", attributes[0]);
        if(lProductCatagory.size() == 0 ){
            this.category = new ProductCategory(attributes[0],"");
            this.addProductCategoryFlag = true;
        }
        else
        {
            this.category = lProductCatagory.get(0);
        }

        List <Product> lResult = Select.from(Product.class).where(Condition.prop("english_name").eq(attributes[1]),
                Condition.prop("marathi_name").eq(attributes[2]),
                Condition.prop("next_month").eq(attributes[3]),
                Condition.prop("quantity").eq(attributes[4]),
                Condition.prop("price").eq(attributes[5])).list();

        if(lResult.size() != 0){
            addProductFlag = false;
        }

        this.englishName = attributes[1];
        this.marathiName = attributes[2];
        this.nextMonth = attributes[3];
        this.quantity = attributes[4];
        this.price = attributes[5];

        Log.d("Debugging", "Category added to :" + this.category.getProductCategory());
    }

    @Override
    public void save() {
        if (this.addProductCategoryFlag == true)
            this.category.save();
        if (this.addProductFlag ==true)
            super.save();
    }

    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getNextMonth() {
        return nextMonth;
    }

    public void setNextMonth(String nextMonth) {
        this.nextMonth = nextMonth;
    }

    public String getMarathiName() {
        return marathiName;
    }

    public void setMarathiName(String marathiName) {
        this.marathiName = marathiName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }
}
