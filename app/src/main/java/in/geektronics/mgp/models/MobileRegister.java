package in.geektronics.mgp.models;

/**
 * Created by varunbhat on 13/03/15.
 */
public class MobileRegister {
    private static final String JSON_KEY_ANDROID_ID = "android_id";
    private static final String JSON_KEY_PHONE_NUMBER = "phone";
    private static final String JSON_KEY_IMEI = "imei";
    private String imei;
    private String phoneNumber;
    private String androidDeviceId;
    private String verificationCode;

    public MobileRegister(String imei, String phoneNumber, String androidDeviceId) {
        this.imei = imei;
        this.phoneNumber = phoneNumber;
        this.androidDeviceId = androidDeviceId;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAndroidDeviceId() {
        return androidDeviceId;
    }

    public void setAndroidDeviceId(String androidDeviceId) {
        this.androidDeviceId = androidDeviceId;
    }

    @Override
    public String toString() {
        return "MobileModel:{" +
                "imei:'" + getImei() + "'," +
                "mobile:'" + getPhoneNumber() + "'," +
                "android_id:'" + getAndroidDeviceId() + "'" +
                "}";
    }
}
