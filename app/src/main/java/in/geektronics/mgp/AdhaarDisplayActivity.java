package in.geektronics.mgp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;

import butterknife.ButterKnife;
import butterknife.InjectView;
import in.geektronics.mgp.models.User;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class AdhaarDisplayActivity extends ActionBarActivity {
    User mUser;

    @InjectView(R.id.textView_aadhaarNumber)
    TextView adhaarNumberView;

    @InjectView(R.id.textView_name)
    TextView nameView;

    @InjectView(R.id.textView_yob)
    TextView yobView;

    @InjectView(R.id.textView_gender)
    TextView genderView;

    @InjectView(R.id.textView_dob)
    TextView dobView;

    @InjectView(R.id.textView_address)
    TextView addressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath(getString(R.string.default_font))
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
        setContentView(R.layout.activity_adhaar_display);
        ButterKnife.inject(this);
        Intent intent = getIntent();
        mUser = User.findById(User.class, intent.getLongExtra("USER_ID", 0));

        if (mUser.getUid() == null) {
            new IntentIntegrator(this).initiateScan();
        } else {
            setUiData(mUser);
        }
    }

    void setUiData(User user){
        if (user.getUid() == null){
            finish();
        } else {
            adhaarNumberView.setText(user.getUid());
            nameView.setText(user.getName());
            yobView.setText(user.getYob());
            genderView.setText(user.getGender());
            dobView.setText(user.getDob());
            addressView.setText(user.getAddress());
        }

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_adhaar_display, menu);
        return true;
    }

    private User parseAadhaarXML(String data) throws XmlPullParserException, IOException {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        XmlPullParser xpp = factory.newPullParser();


        xpp.setInput(new StringReader(data));
        xpp.next();

        if (xpp.getEventType() != XmlPullParser.END_DOCUMENT) {
            if (xpp.getEventType() == XmlPullParser.START_TAG) {
                if (xpp.getName().equals("PrintLetterBarcodeData")) {
                    Log.d(this.getClass().getName(), "Name:" + xpp.getAttributeValue(null, "name"));

                    mUser.setName(xpp.getAttributeValue(null, "name"));
                    mUser.setGender(xpp.getAttributeValue(null, "gender"));
                    mUser.setYob(xpp.getAttributeValue(null, "yob"));
                    mUser.setLocation(xpp.getAttributeValue(null, "loc"));
                    mUser.setCo(xpp.getAttributeValue(null, "co"));
                    mUser.setUid(xpp.getAttributeValue(null, "uid"));
                    mUser.setAddress(xpp.getAttributeValue(null, "house") + "|" +
                                    xpp.getAttributeValue(null, "loc") + "|" +
                                    xpp.getAttributeValue(null, "po") + "|" +
                                    xpp.getAttributeValue(null, "vtc") + "|" +
                                    xpp.getAttributeValue(null, "dist") + "|" +
                                    xpp.getAttributeValue(null, "subdist") + "|" +
                                    xpp.getAttributeValue(null, "state") + "|" +
                                    xpp.getAttributeValue(null, "pc")
                    );
                    mUser.setDob(xpp.getAttributeValue(null, "dob"));
                }
            }
        }

        return mUser;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        Log.d(this.getClass().getName(), "ScanResult:" + result.getContents());

        if(resultCode == RESULT_OK) {
            try {
                User lUser = parseAadhaarXML(result.getContents());
                lUser.save();
                setUiData(lUser);
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
