package in.geektronics.mgp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import in.geektronics.mgp.R;
import in.geektronics.mgp.models.User;

/**
 * Created by ABHIJEET on 26-01-2015.
 */
public class UserAdapter extends ArrayAdapter<User> {

    ArrayList<User> memberList;
    Context mContext;


    public UserAdapter(Context context, ArrayList<User> objects) {
        super(context, R.layout.user_adapter, objects);
        this.mContext = context;
        this.memberList = objects;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.user_adapter, parent, false);
            holder = new ViewHolder(convertView);
            assert convertView != null;
            convertView.setTag(holder);
        }

        holder.memberName.setText(memberList.get(position).getName());
        String details = "";
        if (memberList.get(position).getMobile().length() > 0)
            details = memberList.get(position).getMobile();
        holder.memberDetails.setText(details);

        convertView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                showOptionsDialog(memberList.get(position));
            }
        });

        return convertView;
    }

    public void showOptionsDialog(final User member){

//        String[] options = this.mContext.getResources().getStringArray(R.array.user_options_array);
//        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.mContext,android.R.layout.simple_list_item_1,options);
//        ListView optionsListView = new ListView(this.mContext);
//        optionsListView.setAdapter(adapter);
//
//        final AlertDialog optionsDialog= new AlertDialog.Builder(this.mContext)
//                .setView(optionsListView).show();
//
//        optionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
////                Intent intent = new Intent(mContext, UserOption.class);
////                intent.putExtra("option_id",position);
////                intent.putExtra("member_id",member.getUid());
////                mContext.startActivity(intent);
//                optionsDialog.hide();
//            }
//        });

    }

    // helpers

    static class ViewHolder {
        @InjectView(R.id.textview_member_name)
        TextView memberName;
        @InjectView(R.id.textview_member_details)
        TextView memberDetails;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
