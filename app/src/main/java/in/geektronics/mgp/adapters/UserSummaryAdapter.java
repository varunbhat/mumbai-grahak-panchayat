package in.geektronics.mgp.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;
import in.geektronics.mgp.R;
import in.geektronics.mgp.models.ProductOrder;

/**
 * Created by varunbhat on 22/03/15.
 */

public class UserSummaryAdapter extends ArrayAdapter<ProductOrder> {
    private ArrayList<ProductOrder> mProductList;
    private Context mContext;
    long mProductCount = 0;
    float mTotalAmount = 0;


    public UserSummaryAdapter(Context context, ArrayList<ProductOrder> objects) {
        super(context, R.layout.list_shopping_summary, objects);
        this.mContext = context;
        this.mProductList = objects;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        LayoutInflater inflater = LayoutInflater.from(mContext);

        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = inflater.inflate(R.layout.list_shopping_summary, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        holder.productName.setText(mProductList.get(position).getProduct().getEnglishName());
        holder.productCount.setText(mContext.getResources().getString(R.string.user_report_item_prepend)+mProductList.get(position).getQuantity());
        holder.productCost.setText(mContext.getResources().getString(R.string.user_report_cost_prepend) +
                (new Float(mProductList.get(position).getQuantity()) *
                        new Float(mProductList.get(position).getProduct().getPrice())));



        Log.d("debug","Total Product :"+ mProductCount);
        Log.d("debug","Total Price   :" + mTotalAmount);

//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                holder.memberContactSelected.setChecked(!holder.memberContactSelected.isChecked());
//                memberList.get(position).setSelected(holder.memberContactSelected.isChecked());
//            }
//        });
//
//        holder.memberContactSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                memberList.get(position).setSelected(isChecked);
//            }
//        });

        return convertView;
    }

    public long getProductCount(){
        Log.d("debug","Calling GetProduct Count "  + mProductCount);
        return this.mProductCount;
    }

    public float getTotalCost(){
        Log.d("debug","Calling Get Cost "  + mTotalAmount);
        return this.mTotalAmount;
    }

    static class ViewHolder {
        @InjectView(R.id.product_name)
        TextView productName;
        @InjectView(R.id.product_count)
        TextView productCount;
        @InjectView(R.id.item_cost)
        TextView productCost;

        public ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
