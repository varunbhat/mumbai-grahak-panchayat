package in.geektronics.mgp.util;

import android.os.Bundle;

import in.geektronics.mgp.models.User;

public interface OnLoginInteractionListener {
    public void onNextButtonPress(Bundle data);

    public void registerUser(User user);
}