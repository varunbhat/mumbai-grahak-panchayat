package in.geektronics.mgp;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import in.geektronics.mgp.adapters.ContactAdaper;
import in.geektronics.mgp.models.GrahakGroup;
import in.geektronics.mgp.models.GroupRegistrations;
import in.geektronics.mgp.models.User;
import in.geektronics.mgp.util.NetUtils;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class CreateGroupActivity extends ActionBarActivity {
    private ContactAdaper adapter;
    private ArrayList<User> userList;
    long mGroupId = -1;

    @InjectView(R.id.group_name)
    EditText mGroupNameView;

    @InjectView(R.id.contact_list)
    ListView contactsList;

    @InjectView(R.id.contact_retrival_progress)
    RelativeLayout contactProgress;

    @OnClick(R.id.add_group)
    public void addGroup() {
        GrahakGroup lGrahakGroup;
        Log.d("debug", "" + adapter.getContactList().size());
        Log.d("debug", "Group ID check:" + mGroupId);

        if (mGroupId == -1) {
            String lGroupId = mGroupNameView.getText().toString();

            if (!lGroupId.matches("^ *[\\d]+ *$")) {
                mGroupNameView.requestFocus();
                mGroupNameView.setError("Enter Valid Group ID");
                return;
            } else {
                List<GrahakGroup> lGrahakGroupRes = GrahakGroup.find(GrahakGroup.class, "group_id=?", lGroupId);
                if (lGrahakGroupRes.size() == 0) {
                    mGroupNameView.setError("No Such Group Exists");
                    return;
                } else {
                    lGrahakGroup = lGrahakGroupRes.get(0);
                }
            }
        } else {
            Log.d("debug", "Called from activity with Group ID:" + mGroupId);
            List<GrahakGroup> lGrahakGroupRes = GrahakGroup.find(GrahakGroup.class, "group_id=?", ""+ mGroupId);
            if (lGrahakGroupRes.size() == 0) {
                Log.e("debug", "Called from activity with Group ID:" + mGroupId);
                mGroupNameView.setError("No Such Group Exists");
                return;
            } else {
                lGrahakGroup = lGrahakGroupRes.get(0);
            }
        }

        ArrayList<String> lMobileConfirmationNumbers = new ArrayList<>();


        // Get All Selected Contacts
        ArrayList<User> lSelectedContactList = adapter.getContactList();

        if(lSelectedContactList.size() == 0){
            Toast.makeText(getApplicationContext(),"No Contact Selected",Toast.LENGTH_LONG).show();
            return;
        }

        for ( User lContact : lSelectedContactList){
            Log.d("CreateGroup DB Update","Name:"+lContact.getName()+" Contact:" + lContact.getMobile().replaceAll("[^\\d+]",""));

            List <User> data = User.find(User.class, "mobile=?", lContact.getMobile().replaceAll("[^\\d+]",""));

            Log.d("CreateGroupDB","FoundSize:" + data.size());
            User changeContact;

            if (data.size() > 0){
                changeContact = data.get(0);
                changeContact.setName(lContact.getName());
                changeContact.save();
            }
            else {
                changeContact = new User(lContact.getName(),lContact.getMobile().replaceAll("[^\\d+]",""));
                changeContact.save();
                lMobileConfirmationNumbers.add(lContact.getMobile());
            }

            if(GroupRegistrations.find(GroupRegistrations.class,
                    "contact=? and grahak_group=?",
                    changeContact.getId().toString(),
                    lGrahakGroup.getId().toString()).size()==0) {
                new GroupRegistrations(changeContact, lGrahakGroup).save();
            }
            else {
                Log.d("CreateGroupDB","Group:" + lGrahakGroup.getGroupName() + ", Contact " + changeContact.getMobile() + "already Present");
            }
        }

        lGrahakGroup.setIsAdmin(true);
        lGrahakGroup.save();

//        if (!NetUtils.isOnline(getApplicationContext())) {
//            // Todo: Update Online! (Or use sync service)

            Log.d("CreateGroupDB","Sending sms");
            for (String lMobNum : lMobileConfirmationNumbers) {
                Log.d("debug", "Sending Sms " + lMobNum);

                if (lMobNum.length() >= 10) {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(lMobNum, null, "Hello, I have added you to the Mumbai Grahak Panchayat Group." +
                            "To confirm your addition, Please reply with YES.", null, null);
                }
            }
//        } else {
//            Log.d("CreateGroupDB","Internet is online");
//        }

        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath(getString(R.string.default_font))
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );

        setContentView(R.layout.activity_create_group);
        ButterKnife.inject(this);

        Intent intent = getIntent();

        mGroupId = intent.getLongExtra("GROUP_ID",-1);

        if(mGroupId!=-1)
            mGroupNameView.setVisibility(View.GONE);


        userList = getAllContacts();
        adapter = new ContactAdaper(getApplicationContext(), userList);
        Log.d("Debugging", "" + adapter.toString());
        contactsList.setAdapter(adapter);

        contactProgress.setVisibility(View.GONE);
        contactsList.setVisibility(View.VISIBLE);

        if (!GrahakGroup.findAll(GrahakGroup.class).hasNext())
            new GrahakGroup("Test Group", new Long(1)).save();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mgp_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    private User get(Uri userUri) {
        User newUser = null;
        Cursor c = getContentResolver().query(userUri, null, null, null, null);

        if (c.getCount() > 0 && c.moveToFirst()) {

            String id = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
            String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            String phoneNumber = "";

            if (Integer.parseInt(c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                Cursor pCur = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                        new String[]{id}, null);
                while (pCur.moveToNext()) {
                    // Do something with phones
                    phoneNumber = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA));
                    Log.d(this.getClass().getName(), "name:" + name + "PhoneNumber:" + phoneNumber);
                }
                pCur.close();
            }

            newUser = new User(name, null, null, null, phoneNumber);
        }
        return newUser;
    }


    public ArrayList<User> getAllContacts() {
        boolean isFirst = true;
        boolean querySuccess = false;
        int queryCount;
        ArrayList<User> userList = new ArrayList<>();

        Cursor c = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        queryCount = c.getCount();

        while (true) {
            if (queryCount == 0) {
                c.close();
                break;
            } else if (isFirst) {
                querySuccess = c.moveToFirst();
                isFirst = false;
            } else if (!isFirst) {
                querySuccess = c.moveToNext();
                if (c.isAfterLast()) {
                    c.close();
                    break;
                }
            }

            if (querySuccess) {
                String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                String phoneNumber = "";
                Bitmap image;

                if (Integer.parseInt(c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    phoneNumber = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA));
                    //Log.d(this.getClass().getName(), "name:" + name + " PhoneNumber:" + phoneNumber);
                }
                userList.add(new User(name, phoneNumber));
            }
        }
        return userList;
    }

    public InputStream openPhoto(long contactId) {
        Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
        Uri photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);
        Cursor cursor = getContentResolver().query(photoUri,
                new String[]{ContactsContract.Contacts.Photo.PHOTO}, null, null, null);
        if (cursor == null) {
            return null;
        }
        try {
            if (cursor.moveToFirst()) {
                byte[] data = cursor.getBlob(0);
                if (data != null) {
                    return new ByteArrayInputStream(data);
                }
            }
        } finally {
            cursor.close();
        }
        return null;
    }
}
