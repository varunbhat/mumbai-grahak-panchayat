package in.geektronics.mgp.fragments;


import android.content.Intent;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import in.geektronics.mgp.CreateGroupActivity;
import in.geektronics.mgp.R;

public class NewGroupFragment extends Fragment {
    @OnClick(R.id.group_add)
    void newGroupCreate(){
        Intent intent = new Intent(getActivity().getApplicationContext(),CreateGroupActivity.class);
        startActivity(intent);
    }

    public NewGroupFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_create_group_prompt, container, false);
        ButterKnife.inject(this,view);
        return view;
    }
}
