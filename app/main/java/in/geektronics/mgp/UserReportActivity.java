package in.geektronics.mgp;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import in.geektronics.mgp.adapters.UserSummaryAdapter;
import in.geektronics.mgp.models.Product;
import in.geektronics.mgp.models.ProductCategory;
import in.geektronics.mgp.models.ProductOrder;
import in.geektronics.mgp.models.User;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class UserReportActivity extends ActionBarActivity {
    List<ProductOrder> mUserList ;
    UserSummaryAdapter mUserSummaryAdapter;

    @InjectView(R.id.product_cart)
    ListView mProductList;

    @InjectView(R.id.total_item_count)
    TextView mTotalItemCountView;

    @InjectView(R.id.total_cost)
    TextView mTotalCostView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath(getString(R.string.default_font))
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
        setContentView(R.layout.activity_user_report);
        ButterKnife.inject(this);

        mUserList = Select.from(ProductOrder.class).where(Condition.prop("user").eq(getIntent().getLongExtra("USER_ID",0))).list();

        mUserSummaryAdapter = new UserSummaryAdapter(getApplicationContext(),new ArrayList<>(mUserList));
        mProductList.setAdapter(mUserSummaryAdapter);

        int lProductCount = 0;
        float lTotalAmount = 0;

        for (ProductOrder lProductOrder : mUserList) {
            lProductCount += lProductOrder.getQuantity();
            lTotalAmount += new Float(lProductOrder.getQuantity()) *
                    new Float(lProductOrder.getProduct().getPrice());
        }

        mTotalItemCountView.setText("Total Items: " + lProductCount);
        mTotalCostView.setText("Total Amount: Rs." + lTotalAmount);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_user_report, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
