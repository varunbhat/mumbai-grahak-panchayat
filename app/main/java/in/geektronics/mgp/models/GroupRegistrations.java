package in.geektronics.mgp.models;

import com.orm.SugarRecord;

/**
 * Created by varunbhat on 11/04/15.
 */
public class GroupRegistrations extends SugarRecord<GroupRegistrations> {
    User contact;
    GrahakGroup grahakGroup;
    Boolean isVerified;

    public GroupRegistrations() {
    }

    public GroupRegistrations(User contact, GrahakGroup grahakGroup) {
        this.contact = contact;
        this.grahakGroup = grahakGroup;
        this.isVerified = false;
    }

    public User getContact() {
        return contact;
    }

    public void setContact(User contact) {
        this.contact = contact;
    }

    public GrahakGroup getGrahakGroup() {
        return grahakGroup;
    }

    public void setGrahakGroup(GrahakGroup grahakGroup) {
        this.grahakGroup = grahakGroup;
    }

    public Boolean getIsVerified() {
        return isVerified;
    }

    public void setIsVerified(Boolean isVerified) {
        this.isVerified = isVerified;
    }
}
