package in.geektronics.mgp.models;

import com.orm.SugarRecord;
import com.orm.dsl.Ignore;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ABHIJEET on 22-01-2015.
 */
public class ProductCategory extends SugarRecord<ProductCategory> {
    String gid;
    String productCategory;
    String productCategoryTranslation;

    @Ignore
    List<Product> lProductGroups;

    @Ignore
    private static final String JSON_KEY_GID = "grpid";
    @Ignore
    private static final String JSON_KEY_GROUP_NAME = "name";
    @Ignore
    private static final String JSON_KEY_GROUP_TITLE = "translation";

    public ProductCategory() {
    }

    public ProductCategory(String productCategory, String productCategoryTranslation) {
        this.productCategory = productCategory;
        this.productCategoryTranslation = productCategoryTranslation;
    }

    public ProductCategory(String gid, String productCategory, String productCategoryTranslation) {
        this.gid = gid;
        this.productCategory = productCategory;
        this.productCategoryTranslation = productCategoryTranslation;
    }

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getProductCategoryTranslation() {
        return productCategoryTranslation;
    }

    public void setProductCategoryTranslation(String productCategoryTranslation) {
        this.productCategoryTranslation = productCategoryTranslation;
    }



    // For returning JSON object
    public static ProductCategory fromJson(JSONObject jsonObject) {
//        ProductGroup g = new ProductGroup();
//        try {
//            // Deserialize json into object fields
//            g.setGid(jsonObject.getString(JSON_KEY_GID));
//            g.setGroupTitle(jsonObject.getString(JSON_KEY_GROUP_TITLE));
//            g.setGroupName(jsonObject.getString(JSON_KEY_GROUP_NAME));
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//            return null;
//        }
//        // Return new object
//        return g;
        return null;
    }


    public static ArrayList<ProductCategory> fromJson(JSONArray jsonArray) {
//        ArrayList<ProductGroup> groups = new ArrayList<ProductGroup>(jsonArray.length());
//        // Process each result in json array, decode and convert to groups
//        // object
//        for (int i = 0; i < jsonArray.length(); i++) {
//            JSONObject groupJson = null;
//            try {
//                groupJson = jsonArray.getJSONObject(i);
//            } catch (Exception e) {
//                e.printStackTrace();
//                continue;
//            }
//
//
//            ProductGroup group = ProductGroup.fromJson(groupJson);
//            if (group != null) {
//                groups.add(group);
//            }
//        }
//        return groups;
        return null;
    }

    @Override
    public String toString() {
        return "ProductGroup{" +
                "groupName='" + productCategory + '\'' +
                ", translate='" + productCategoryTranslation + '\'' +
                '}';
    }
}
