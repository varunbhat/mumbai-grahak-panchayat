package in.geektronics.mgp.models;

import com.orm.SugarRecord;

/**
 * Created by varunbhat on 11/04/15.
 */
public class GrahakGroup extends SugarRecord<GrahakGroup> {

    String groupName;
    Long groupID;
    Boolean isAdmin;
    Boolean isMember;


    public Boolean getIsMember() {
        return isMember;
    }

    public void setIsMember(Boolean isMember) {
        this.isMember = isMember;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Long getGroupID() {
        return groupID;
    }

    public void setGroupID(Long groupID) {
        this.groupID = groupID;
    }

    public Boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(Boolean isAdmin) {
        this.isAdmin = isAdmin;
    }
    public GrahakGroup(){

    }

    public GrahakGroup(String name, Long id){
        this.groupName = name;
        this.groupID = id;
        this.isAdmin = false;
        this.isMember = false;
    }

    public String toString(){
        return this.groupName;
    }
}
