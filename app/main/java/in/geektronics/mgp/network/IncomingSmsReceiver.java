package in.geektronics.mgp.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

import in.geektronics.mgp.models.GroupRegistrations;
import in.geektronics.mgp.models.User;
import in.geektronics.mgp.util.OnLoginInteractionListener;

/**
 * Created by varunbhat on 15/03/15.
 */

public class IncomingSmsReceiver extends BroadcastReceiver {
    final SmsManager sms = SmsManager.getDefault();
    private OnLoginInteractionListener mListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        final Bundle bundle = intent.getExtras();
        //mListener = (OnLoginInteractionListener) context;

        try {
            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");


                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();

                    Log.i("SmsReceiver", "senderNum: " + senderNum + "; message: " + message);

                    if(message.toUpperCase().equals("YES")) {
                        List <User> lUser = User.find(User.class, "mobile like ?", "%"+senderNum);
                        if (lUser == null) {
                            Log.d("debug", "No Contact found");
                            return;
                        } else {
                            Log.d("debug","The message userid:" + lUser.get(0).getId());

                            GroupRegistrations lUserVerify = Select.from(GroupRegistrations.class)
                                    .where(Condition.prop("contact").eq(lUser.get(0).getId())).first();

                            if (lUserVerify == null) {
                                Log.d("debug", "No User With conditions Satisfied");
                                return;
                            } else {
                                Log.d("debug", "User Successfully Verified");
                                lUserVerify.setIsVerified(true);
                                lUserVerify.save();
                            }
                        }
                    }
                    else {
                        Log.d("debug", "Content Not Proper");
                    }

                    // Show Alert
                    int duration = Toast.LENGTH_LONG;
                    Toast toast = Toast.makeText(context,
                            "senderNum: " + senderNum + ", message: " + message, duration);
                    toast.show();

                } // end for loop
            } // bundle is null

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" + e);
        }
    }
}