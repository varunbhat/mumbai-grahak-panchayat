package in.geektronics.mgp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import in.geektronics.mgp.adapters.GroupMembersListAdapter;
import in.geektronics.mgp.models.GroupRegistrations;
import in.geektronics.mgp.models.ProductOrder;
import in.geektronics.mgp.models.User;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class ListMembersActivity extends ActionBarActivity {
    Long mGroupId = -1L;

    ArrayList<User> mGroupUsers;
    GroupMembersListAdapter mGroupMembersListAdapter;

    @InjectView(R.id.user_list)
    ListView mUserListView;

    @OnClick(R.id.submit_button)
    void finalSubmit(){
        for (GroupRegistrations lGroupRegistrant: GroupRegistrations.find(GroupRegistrations.class,"grahak_group=?","" + mGroupId)) {
            float lTotalAmount = 0;
            for (ProductOrder lProductOrder : Select.from(ProductOrder.class).where(
                    Condition.prop("user").eq(lGroupRegistrant.getContact().getId())).list()) {
                lTotalAmount += new Float(lProductOrder.getQuantity()) *
                        new Float(lProductOrder.getProduct().getPrice());
            }

            Log.d("debug","User:" + lGroupRegistrant.getContact().getMobile() + " Amt:" + lTotalAmount);

            if(lGroupRegistrant.getContact().getMobile().length()>=10) {
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(lGroupRegistrant.getContact().getMobile(), null,
                        "Thank you for doing business with Mumbai Grahak Panchayat. You are requested to pay Rs." + lTotalAmount + "to your group Leader.", null, null);
            }
        }
    }

    @OnClick(R.id.view_report)
    void viewReport(){
        Intent intent = new Intent(getApplicationContext(),ProductReportActivity.class);
        startActivity(intent);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath(getString(R.string.default_font))
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );

        setContentView(R.layout.activity_list_members);
        ButterKnife.inject(this);

        final Intent intent = getIntent();
        final Bundle data = intent.getExtras();
        mGroupUsers = new ArrayList<>();

        mGroupId = data.getLong("GROUP_ID");

        updateUserList();

        mGroupMembersListAdapter = new GroupMembersListAdapter(getApplicationContext(), mGroupUsers);
        mUserListView.setAdapter(mGroupMembersListAdapter);

        View footerView = getLayoutInflater().inflate(R.layout.list_members_footer, mUserListView, false);
        mUserListView.addFooterView(footerView);

        mUserListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("Debug","List Item Number:"+ position);

                if (position< mGroupUsers.size()) {
                    Intent intent = new Intent(getApplicationContext(), ShoppingListActivity.class);
                    intent.putExtra("USER_ID", mGroupUsers.get(position).getId());
                    startActivity(intent);
                 } else {
                    Toast.makeText(getApplicationContext(),"Need to add new members",Toast.LENGTH_SHORT).show();
                    Intent intent1 = new Intent(getApplicationContext(),CreateGroupActivity.class);
                    intent1.putExtra("GROUP_ID",new Long(data.getLong("GROUP_ID")));
                    startActivity(intent1);
                }
            }
        });
    }


    private void  updateUserList(){
        mGroupUsers.clear();

        List<GroupRegistrations> lGroupData = GroupRegistrations.find(GroupRegistrations.class,
                "grahak_group=?", mGroupId.toString());

        for (GroupRegistrations i : lGroupData) {
            mGroupUsers.add(i.getContact());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mgp_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        Log.d("debug","OnResume called");

        updateUserList();
        mGroupMembersListAdapter.notifyDataSetChanged();

        super.onResume();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
